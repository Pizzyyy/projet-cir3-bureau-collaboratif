
const MongoClient = require('mongodb').MongoClient
const ip = "10.224.2.35";
const url = 'mongodb://'+ ip + ':27017';
//const url = 'mongodb+srv://kapesteon:admin@cluster0.in2bi.mongodb.net/DB?retryWrites=true&w=majority';
const client = new MongoClient(url,{
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
const connection = client.connect() // initialized connection


module.exports={

        



















    /*----------------------------------------------*/
    /*-------------PARTIE USER----------------------*/
    /*----------------------------------------------*/
    
    /*
        getListUsersByEntreprise()
        getUserById()
        addUser()
        updateUser()
    */

    /**
     *  getListUsersByEntreprise
     *  Retrieves every user with for a given entreprise
     * 
     *
     * 
     * @param {string} entreprise Entreprise's name
     * @returns {Promise} Promise -- will later be an array of users with "status"
     *                    sorted in alphabetical order 
     */

    getListUsersByEntreprise(entreprise){
        if (entreprise==null){
            return ("Error : Entreprise is null")
        }

        const document = { entreprise:entreprise }
        const sort = {status : 1} // alphabetical order
        const db = client.db('DB')
        const coll = db.collection('user')
        return new Promise(function (resolve, reject) {
            try{
                result = coll.find(document).sort(sort).toArray()
                return resolve(result) 
            }
            catch(e){
                return reject(e)
            }

        });
    },


/**
     *  getUserById
     *  Retrieves a specific user given by it's id
     * 
     *
     * @param {string} id user's id 
     * @returns {Promise} Promise -- will later be a chat Object 
     */
    getUserById(id){
        const document = { id:id }
        const db = client.db('DB')
        const coll = db.collection('user')
        return new Promise(function (resolve, reject) {
            try{
                result = coll.findOne(document)
                return resolve(result) 
            }
            catch(e){
                return reject(e)
            }

        });
    
 
    },



    /**
     * addUser
     * Insert in the database a new user, most likely used on the login page
     * 
     * @param {string} id user's id 
     * @param {string} name user's lastname
     * @param {string}  firstName user's firstname
     * @param {string} email user's email
     * @param {string} entreprise user's entreprise
     * @param {string} tel  user's phone number
     * @param {string} status user's status (CASE SENSITIVE), either
     *                         "En ligne"
     *                         "Inactif"
     *                         "Ne pas deranger"
     *                         "Hors ligne"
     * @param {string} role user's role (CASE SENSITIVE), either
     *                          "Admin"
     *                          "Member"
     * @param {string} pwd user's password unhashed

     * @returns {void}

     */

    addUser(id,name,firstName,email,entreprise,tel,status,role,pwd){
        const db = client.db('DB');
        const coll = db.collection('user');
        const documentToInsert = {
            id:id,
            nom:name,
            prenom:firstName,
            email:email,
            entreprise:entreprise,
            tel:tel,
            status:status,
            role:role,
            mdp:pwd
        }
        coll.insertOne(documentToInsert);

    },


    /**
     *  updateUser
     *  updates a user for a given id with data
     * 
     *
     * 
     * @param {string} id user's id 
     * @param {Array} data      
     * Update any field given in the parameter "data" 
     * for a specified user
     * 
     * Data looks like : 
     * let data ={ 
     * 
     *   nom:"name",      
     *   prenom:"firstname",  
     *   email:mail,    
     *   entreprise:"entreprise", 
     *   tel:"tel",   
     *   status:"Inactif",  
     *   role:"role", 
     *   }
     * 
     * Data must NOT necessarily contain every field
     * 
     * @returns {void}
     */

    updateUser(id, data){
        const document = {id:id};
        const operation = {$set:data};
        const db = client.db('DB');
        const coll = db.collection('user');
        coll.updateOne(document,operation)
    },

    getListUsersEmail(email){ // de la forme mmmmmm@dysack.com
        if (email==null){
            return ("Error : email is null")
        }

        const document = { email:email }
        const sort = {id : 1} // alphabetical order
        const db = client.db('DB')
        const coll = db.collection('user')
        return new Promise(function (resolve, reject) {
            try{
                //let result = coll.find(document).sort(sort).toArray()
                let result = coll.find(document).sort(sort).toArray()
                return resolve(result) 
                
            }
            catch(e){
                return reject(e)
            }

        });
    },
    






















    /*----------------------------------------------*/
    /*-------------PARTIE CHAT----------------------*/
    /*----------------------------------------------*/
    /*

    createChat()       
    addMsgToChat()     
    getChatById()       
    getChatByUsers()

    */

    /**
     *  createChat
     *  creates a new chat with a pre-generated message
     * 
     *
     * 
     * @param {string} id chat's id 
     * @param {string} chatName chat's name
     * @param {Array}   users array of users participating in this chat,
     *                  only the id,firstname and lastname are required, looks like :
     * 
     *                  id:id,
     *                  name:"firstname",
     *                  lastname:"lastname",
     * 
     * @param {string}  day day of creation of the chat, must looks like
     *                 dd/mm/yyyy
     * @param {string}  hour hour of creation of the chat, must looks like
     *                   XXhXX
     * @returns {void}
     */

    createChat(id,chatName,users,day,hour){      
        users.sort(this.cmpUsersId);
        const db = client.db('DB')
        const coll = db.collection('chat')
        const documentToInsert = {
            id:id,
            nom:chatName,
            users:users,
            messages:[
                {
                content:"Début de votre conversation",
                author:"System",
                date:{jour:day,heure:hour}
                }
            ]
        }
        coll.insertOne(documentToInsert);

    },


    /**
     *  addMsgToChat
     *  appends a message to a given chat, most likely called every time a
     *  user send a message in a chat
     * 
     *
     * 
     * @param {string} id chat's id 
     * @param {Array} data message's data, looks like:
     * 
     *              content : "slt",
     *              author :"Jean",
     *              date :{jour:"00/00/2021",heure:"12h45"}
     * 
     * @returns {void}
     */

    addMsgToChat(id,data){     //called every time a chat message is send 

        try{
            const document = {id:id}
            const db = client.db('DB')
            const coll = db.collection('chat')

            const operation = { $push: {messages: data} }

            coll.updateOne(document,operation)
        }
        catch(e){
            console.log(e)
        }

    },

    /**
     *  getChatById
     *  Retrieves a specific chat given by it's id
     * 
     *
     * @param {string} id chat's id 
     * @returns {Promise} Promise -- will later be a chat Object 
     */
    getChatById(id){
        const document = { id:id }
        const db = client.db('DB')
        const coll = db.collection('chat')
        return new Promise(function (resolve, reject) {
            try{
                result = coll.findOne(document)
                return resolve(result) 
            }
            catch(e){
                return reject(e)
            }

        });
    
 
    },

    /**
     *  getChatByUser
     *  Retrieves a specific chat given by it's users
     * 
     *
     * @param {string} id chat's id 
     * @param {Array}   users array of users participating in this chat,
     *                  only the id,firstname and lastname are required, looks like :
     * 
     *                  id:id,
     *                  name:"firstname",
     *                  lastname:"lastname",
     * 
     *  @returns {Promise} Promise -- will later be a chat Object
     */
    getChatByUsers(users){//users est un tableau de [{id,nom,prenom},{id2,nom2,prenom2}]
        users.sort(this.cmpUsersId);
        const document = { users:users }
        const db = client.db('DB')
        const coll = db.collection('chat')
        return new Promise(function (resolve, reject) {
            try{
                result = coll.findOne(document)
                return resolve(result) 
            }
            catch(e){
                return reject(e)
            }

        });
    
 
    },


    /**
     *  getListUsers
     *  Retrieves every user with for a given entreprise
     * 
     *
     * 
     * @param {string} entreprise Entreprise's name
     * @returns {Promise} Promise -- will later be an array of users with "status"
     *                    sorted in alphabetical order 
     */

    getListUsersChat(userID){
        if (userID==null){
            return ("Error : Entreprise is null")
        }

        const document = { "users.id":userID };
        const sort = {status : 1}; // alphabetical order
        const db = client.db('DB');
        const coll = db.collection('chat');
        return new Promise(function (resolve, reject) {
            try{
                let result = coll.find(document).sort(sort).toArray();
                return resolve(result) ;
            }
            catch(e){
                return reject(e);
            }

        });
    },






















    /*----------------------------------------------*/
    /*-------------PARTIE PLANNING------------------*/
    /*----------------------------------------------*/
    /*
    addPlanning()
    updatePlanning()
    getPlanningById()
    getPlanningByUsers()
    
    
    */
        /** 
     * @param {string} id planning's id 
     * @param {string} title Title of this task
     * @param {string} msg task planned to do
     * @param {Array}   users array of users participating in this planning
     * @param {string}  day day of creation of the chat, must looks like
     *                 {jour:"dd/mm/yyyy",heure:"XXhXX"}
     *  
     * @param {string} status user's status (CASE SENSITIVE), either
     *                         "Fait"
     *                         "En cours"
     *                         "A faire"       
     * @returns {void}
     */
    addPlanning(id ,title,msg ,users ,date ,status){
        users.sort(this.cmpUsersId);
        const db = client.db('DB')
        const coll = db.collection('planning')
        const documentToInsert = {
            id:id,
            titre:title,
            msg:msg,
            users:users,
            date:date,
            status:status
        }
        coll.insertOne(documentToInsert);
    },

/**
     *  updatePlanning
     *  updates a user for a given id with data
     * 
     *
     * 
     * @param {string} id planning's id 
     * @param {Array} data      
     * Update any field given in the parameter "data" 
     * for a specified user
     * 
     * Data looks like : 
     * let data ={ 
     * 
     *   id:id,      
     *   titre:"title",  
     *   msg:"msg",    
     *   users:[{id,nom,prenom},{id2,nom2,prenom2}],
     *   date: {jour:"dd/mm/yyyy",heure:"XXhXX"},   
     *   status:"Fait", 
     *   }
     * 
     * Data must NOT necessarily contain every field
     * 
     * @returns {void}
     */

    updatePlanning(id, data){
        const document = {id:id};
        const operation = {$set:data};
        const db = client.db('DB');
        const coll = db.collection('planning');
        coll.updateOne(document,operation)
    },



    /**
     * @param {string} id chat's id 
     * @param {string} entreprise Entreprise's name
     * @returns {Promise} Promise -- will later be a planning Object
     */
    getPlanningById(id){
        const document = { id:id }
        const db = client.db('DB')
        const coll = db.collection('planning')
        return new Promise(function (resolve, reject) {
            try{
                result = coll.findOne(document)
                return resolve(result) 
            }
            catch(e){
                return reject(e)
            }

        });
    
 
    },






    /**
     *  getPlanningByUsers
     *  Retrieves a specific planning given by it's users
     * 
     *
     * @param {string} id planning's id 
     * @param {Array}   users array of users participating in this chat,
     *                  only the id,firstname and lastname are required, looks like :
     * 
     *                  id:id,
     *                  name:"firstname",
     *                  lastname:"lastname",
     * 
     *  @returns {Promise} Promise -- will later be a planning Object
     */
    getPlanningByUsers(users){//users est un tableau de [{id,nom,prenom},{id2,nom2,prenom2}]
        users.sort(this.cmpUsersId);
        const document = { users:users }
        const db = client.db('DB')
        const coll = db.collection('planning')
        return new Promise(function (resolve, reject) {
            try{
                result = coll.findOne(document)
                return resolve(result) 
            }
            catch(e){
                return reject(e)
            }

        });
    
 
    },



    /**
     * @param {string} id Planning's id 
     * @returns {void}
     */
    removePlanningById(id){
        const document = { id:id };
        const db = client.db('DB');
        const coll = db.collection('planning');
        coll.deleteOne(document);
    },



/**
     *  getListUsers
     *  Retrieves every user with for a given entreprise
     * 
     *
     * 
     * @param {string} entreprise Entreprise's name
     * @returns {Promise} Promise -- will later be an array of users with "status"
     *                    sorted in alphabetical order 
     */

    getListUsersPlanning(idUser){
        if (idUser==null){
            return ("Error : idUser is null")
        }

        const document = { "users.id":idUser };
        const sort = {status : 1}; // alphabetical order
        const db = client.db('DB');
        const coll = db.collection('planning');
        return new Promise(function (resolve, reject) {
            try{
                let result = coll.find(document).sort(sort).toArray();
                return resolve(result) ;
            }
            catch(e){
                return reject(e);
            }

        });
    },






















    /*----------------------------------------------*/
    /*----------------PARTIE FORUM------------------*/
    /*----------------------------------------------*/

    createForum(id,question,entreprise,day,hour){        
        const db = client.db('DB')
        const coll = db.collection('forum')
        const documentToInsert = {
            id:id,
            question:question,
            entreprise:entreprise,
            messages:[
                {
                content:"Début du forum",
                author:"SYSTEM",
                date:{jour:day,heure:hour}
                }
            ]
        }
        coll.insertOne(documentToInsert);

    },




    addMsgToForum(id,data){     //called every time a forum message is send 

        try{
            const document = {id:id}
            const db = client.db('DB')
            const coll = db.collection('forum')

            const operation = { $push: {messages: data} }

            coll.updateOne(document,operation)
        }
        catch(e){
            console.log(e)
        }

    },



    changeStatusForum(id,status){ 

        try{
            const document = {id:id}
            const db = client.db('DB')
            const coll = db.collection('forum')

            const operation = { $set: {status: status} }

            coll.updateOne(document,operation)
        }
        catch(e){
            console.log(e)
        }

    },




    getListForumByEntreprise(entreprise){
        if (entreprise==null){
            return ("Error : Entreprise is null")
        }

        const document = { entreprise:entreprise }
        const sort = {status : 1} // alphabetical order
        const db = client.db('DB')
        const coll = db.collection('forum')
        return new Promise(function (resolve, reject) {
            try{
                let result = coll.find(document).sort(sort).toArray()
                return resolve(result) 
            }
            catch(e){
                return reject(e)
            }

        });
    },




    getListForumByQuestion(question){
        if (question==null){
            return ("Error : question is null")
        }

        const document = { question:question }
        const sort = {status : 1} // alphabetical order
        const db = client.db('DB')
        const coll = db.collection('forum')
        return new Promise(function (resolve, reject) {
            try{
                let result = coll.find(document).sort(sort).toArray()
                return resolve(result) 
            }
            catch(e){
                return reject(e)
            }

        });
    },















    /*----------------------------------------------*/
    /*-------------------OTHER----------------------*/
    /*----------------------------------------------*/
    
    
    //Used internally to sort users by their ID
    // called in createChat , getChatByUsers, addPlanning
    cmpUsersId(a,b){
        if (a.id > b.id) return 1;
        if (b.id > a.id) return -1;
    
        return 0;
    },


    
    test(){


        const document = { phone:"08" }
        const db = client.db('testDB')
        const coll = db.collection('contacts')
        return new Promise(function (resolve, reject) {
            try{
                result = coll.findOne(document)
                return resolve(result) 
            }
            catch(e){
                return reject(e)
            }

        });
        
    }
 
}