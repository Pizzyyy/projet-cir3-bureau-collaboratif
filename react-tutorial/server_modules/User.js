class User{
    constructor(data){
        this.id         =  data.id
        this.nom        =  data.nom
        this.prenom     = data.prenom
        this.email      = data.email
        this.entreprise = data.entreprise
        this.tel        = data.tel
        this.status     = data.status
        /*
        En ligne
        Inactif
        Ne pas deranger
        Hors ligne  
        */
        this.role       =  data.role
        /*
        admin
        membre
        */
        this.mdp        = data.mdp

    }
    updateAllInfo(data){
        this.id         =  data.id
        this.nom        =  data.nom
        this.prenom     = data.prenom
        this.email      = data.email
        this.entreprise = data.entreprise
        this.tel        = data.tel
        this.status     = data.status
        /*
        En ligne
        Inactif
        Ne pas deranger
        Hors ligne  
        */
        this.role       =  data.role
        /*
        admin
        membre
        */
        this.mdp        = data.mdp
    }

}

module.exports = User