*Main*


*Users*
--addUser(...)          //Called when a new client goes to the "Sign in" part
--removeUserById(...)   //Used ???
--updateUser(...)       //Used ???
--getListUser(name = null, lastname = null ...)     // Retrieves all the users with the associated info given in paramaters
                                                // if no info is given then it returns every users
                                                // We use default values in this function so that we have only one generic function 
                                                // instead of multiples one


*Chat*
--createChat(...)       //called once for a new conversation between multiple people 
--addMsgToChat(...)     //called every time a chat message is send 
--removeChatById(...)         //used ???
--getChatById()       
--getChatByUsers()


*Planning*
--addPlanning(...)
--updatePlanning(...)
--getPlanningbyId(...)
--getPlanningbyUsers(...)
--removePlanningById(...)

*Partage fichier*
addFichier()