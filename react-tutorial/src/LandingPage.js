import React from 'react';
import { useHistory } from "react-router-dom";
import Brique from './Brique'

class LandingPage extends React.Component{
    render() {
        const {nomPersonnage} = this.props;
        console.log(nomPersonnage);
        console.log("Arrivee sur LandingPage");
    
    return(<table>{Array.from(

        Array(8),
        (e,i) =>{
            return <tr><Brique key={i} /> {
                nomPersonnage[i]
            }</tr> 

        })}</table>)
    }
}

export default LandingPage