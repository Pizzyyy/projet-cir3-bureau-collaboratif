import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from './Login';
import Nav from './Nav';
import Home from './Home';
import Signin from './Signin'
import io from 'socket.io-client'


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { socket: null }
    }

    componentDidMount() {
    }

    render() {

        return (
            <Router>
                <div className="mainPage">
                    <Switch>
                        <Route path="/Nav" component={Nav} />
                        <Route path="/Login" component={() => <Login socket={this.state.socket} />} />
                        <Route path="/" exact component={() => <Home socket={this.state.socket} />} />
                        <Route path="/SignIn" component={Signin} />
                    </Switch>
                </div>
            </Router>
        )
    }
}


export default App