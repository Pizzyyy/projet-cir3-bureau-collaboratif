import React from 'react';
import Popover from 'react-bootstrap/Popover';
import Row from 'react-bootstrap/Row';
import Button from "react-bootstrap/Button";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faUserCircle
} from "@fortawesome/free-solid-svg-icons";

class ContactList extends React.Component {
    render(

    ) {
        let { nomPersonnage } = this.props;

        let listTEMP = new Array()
        let listTEMP2 

        listTEMP2 = nomPersonnage

        if (listTEMP2 !== undefined){
            console.log(listTEMP2)
            listTEMP2.forEach(element => {
                
                listTEMP.push(element.nom);
            });
            console.log(listTEMP);
        }else{
            nomPersonnage = "No contact found !"
            listTEMP = ["No contact found !"]
        }




        

        console.log(listTEMP)
        return (<table><tbody>{Array.from(
            Array(listTEMP.length),
            (e, i) => {
                return (<tr key={i}>                     
                <Row>
                    <OverlayTrigger
                        trigger="hover"
                        placement="right"
                        overlay={       
                            <Popover>
                                <Popover.Title as="h3">{nomPersonnage[i].prenom} {nomPersonnage[i].nom} </Popover.Title>
                                <Popover.Content>

                                {nomPersonnage[i].email} <br/>
                                {nomPersonnage[i].tel} <br/>
                                {nomPersonnage[i].entreprise} <br/><br/>
                                {nomPersonnage[i].status} 
                                    </Popover.Content>
                            </Popover>
                        }
                    >                        
                        <Button variant="success" id="contacts" block><FontAwesomeIcon icon={faUserCircle} /> {listTEMP[i]}</Button>
                    </OverlayTrigger>
                </Row>
                </tr>
                )

            })}</tbody></table>)
    }
}

export default ContactList