import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import "./Login.css";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Link } from 'react-router-dom';
import Nav from "react-bootstrap/Nav";
import Card from "react-bootstrap/Card";

//const existedAccount = true;  //L'utilisateur existe et est amené à la page d'accueil (fct handleSubmit)

import io from 'socket.io-client';



/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
/*-------------------------LOGIN PART------------------------*/
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

//var connection = document.getElementById('connection');
//connection.addEventListener('click', connect);





export default function Login() {


  let loc = 'http://localhost:8000'
  const socket = io.connect(loc);
  var isAuthSuccessful = null
  var reason = null




  socket.on("isAuthSuccessful", (bool, rsn, ID) => {
    submitForm(bool, rsn, ID)
  })



  function sendInfoToDB(email, password) {
    //let password = document.getElementById("password").innerHTML;
    //let email = document.getElementById("email").innerHTML;
    socket.emit("connectionTry", email, password, socket.id)
  }




  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");


  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function submitForm(isAuthSuccessful, reason, ID) {
    console.log("Submitting form... ")
    if (validateForm()) {
      if (isAuthSuccessful) {
        sessionStorage.clear()
        sessionStorage.setItem("clientID",ID);
        window.location.href =  "/";
      }
      else {
        alert(reason)
      }
    }
    else { window.location.href = "/Login"; }

  }


  function handleClick(e) {
    e.preventDefault();
    sendInfoToDB(email, password);
  }

  function redirectRegister(e){
    e.preventDefault();
    window.location.href = "/SignIn";
  }


  return (
    <div id="loginForm">
      <div className="logo">
        <Row className="justify-content-center">
          <Col md="auto">
          </Col>
          <Col md="auto">
            <Image src="dysack.png" height="250" width="250" roundedCircle />
          </Col>
          <Col md="auto">
          </Col>
        </Row>
      </div>
      <div className="Login">
        <Form>
          <Form.Group size="lg" controlId="email">
            <Form.Label>Adresse e-mail </Form.Label>
            <Form.Control
              autoFocus
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}      //Ce que l'utilisateur écrit
            />
          </Form.Group>
          <Form.Group size="lg" controlId="password">
            <Form.Label>Mot de passe </Form.Label>
            <Form.Control
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>

          <Button variant="outline-info" block size="lg" type="submit" onClick={handleClick} disabled={!validateForm()}>
            Se connecter
            </Button>
          <Nav defaultActiveKey="/" className="redirectRegister">
            <Nav.Link onClick={redirectRegister}> Pas de compte ? Créez-en un !</Nav.Link>
          </Nav>
        </Form>
      </div >
    </div >
  );
}