import React from 'react';
import Popover from 'react-bootstrap/Popover';
import Row from 'react-bootstrap/Row';
import Button from "react-bootstrap/Button";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Nav from "react-bootstrap/Nav";
import Home from "./Home.js";

class ChatList extends React.Component {
    render() {
        const { listChat } = this.props;

        
        let listTEMP = new Array()
        let listTEMP2 
        listTEMP2 = listChat
        
        if (listTEMP2[0] === undefined){
            listTEMP2 = [{"users":{"nom":"Nom"}}]
            listTEMP = ["No chat found !"]
        }
        else{


            if (listTEMP2[0].name !== "No chat found !"){
                listTEMP2.forEach(element => {
                    listTEMP.push(element.nom);
                });


            }
            else
            {
    
                listTEMP2 = [{"users":{"nom":"Nom"}}]
                listTEMP = ["No chat found !"]
            }
        }
        return (<div className="chatListElem">{Array.from(
            Array(listTEMP.length),
            (e, i) => {
                  
                return (               
                <OverlayTrigger
                    trigger="hover"
                    placement="left"
                    overlay={                             
                    <Popover>
                        <Popover.Title as="h3">{listTEMP2[i].nom}  </Popover.Title>

                        <Popover.Content >
                        {listTEMP2[i].users[0] !== undefined ? listTEMP2[i].users[0].nom: ""} <br/>
                        {listTEMP2[i].users[1] !== undefined ? listTEMP2[i].users[1].nom: ""} <br/>
                        {listTEMP2[i].users[2] !== undefined ? listTEMP2[i].users[2].nom: ""} <br/>
                        {listTEMP2[i].users[3] !== undefined ? listTEMP2[i].users[3].nom: ""} <br/>
                        </Popover.Content>
                        
                    </Popover>
                }
            > 
                    <Nav.Link key={i} id={listTEMP2[i].id} > {listTEMP[i]}</Nav.Link>
                 
                </OverlayTrigger>
                )
            })}</div>)
    }
}

export default ChatList