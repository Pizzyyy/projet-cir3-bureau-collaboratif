import React, { useState, useCallback, Component, useEffect } from 'react';
import "./Home.css"
import Button from "react-bootstrap/Button";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from 'react-bootstrap/DropdownButton';
import Draggable from "react-draggable";
import Image from "react-bootstrap/Image";
import Nav from "react-bootstrap/Nav";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'
import ContactList from './ContactList'
import Modal from 'react-bootstrap/Modal'
import Calendar from 'react-calendar'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import 'react-calendar/dist/Calendar.css';
import Clock from 'react-clock';
import 'react-clock/dist/Clock.css';
import io from 'socket.io-client'
import ChatList from './ChatList.js'
import ReactStickies from 'react-stickies'; //ES6
import {
    faUpload,
    faPaperPlane,
    faPlusSquare,
    faPowerOff,
    faToggleOn,
    faToggleOff,
    faBellSlash,
    faEye,
    faUserPlus,
    faCheckCircle,
    faComments,
    faCalendarAlt,
    faQuestion,
    faStickyNote
} from "@fortawesome/free-solid-svg-icons";
import { watchFile } from 'fs';


/*import {
    faNpm,
    faBitcoin,
    faInternetExplorer
} from "@fortawesome/free-brands-svg-icons";*/

class Home extends React.Component {
    constructor(props) {
        super(props)
        this.socketHome = io.connect('http://localhost:8000');

        //c'est pour le chat
        this.state = {
            nom: ["Theo", "JC", "Charles", "Valentin", "VDH", "Florent"],
            listC: [{

                "nom": "dummy",
                "users": [{
                    "nom": "dummyName",
                    "prenom": "dummyFirstname"
                }
                ]

            }],
            ListM: [{ nom: "No Contact found" }],
            ListF: [{ question: "No Forum found !", status: "Resolu" }],
            clientInfo: [{ prenom: "Prenom" }, { nom: "Nom" }],
            date: new Date(),
            notes: []
        };
        //this.listChat = [{name:"nop"}]
        //this.listMate = null
        //this.clientInfo = null

        this.onLoadClient()
        this.sendMessage = this.sendMessage.bind(this);
        this.popConversations = this.popConversations.bind(this);

        this.onCreateChat = this.onCreateChat.bind(this);
        this.onChange = this.onChange.bind(this)
        this.onSave = this.onSave.bind(this)

    }

    componentDidMount(prevProps) {

    }
    componentDidUpdate(prevProps) {
        this.toCall(prevProps)
    }

    /*receiveChatList(){
        this.socketHome.emit('requestConversationsForClient',(this.clientInfo.id));
        this.socketHome.on('showConversations', (tempTab)=>{
            let tabChat = tempTab;
        });
        console.log(tabChat);
        return tabChat;
    }*/

    toCall(prevProps) {

        if (prevProps.socket == null && this.socketHome != null) {


            this.socketHome.on('receiveConversation', (conv) => {
                document.getElementById('messages').innerText = "";
                let checkUser
                conv.forEach(message => {
                    if (checkUser != message[1]) {
                        let li = document.createElement('li');
                        let lo = document.createElement('li');
                        li.innerHTML = message[1] + " a " + message[2]
                        document.getElementById('messages').appendChild(li);
                        lo.innerHTML = message[0];
                        document.getElementById('messages').appendChild(lo);
                        checkUser = message[1]
                    }
                    else if (checkUser == message[1]) {
                        let li = document.createElement('li');
                        li.innerHTML = message[0]
                        document.getElementById('messages').appendChild(li);
                    }
                });
            })
        }

    }
    /*
        componentDidMount(){
            if (this.props.socket != null) {
                this.onLoadClient();
            }
        }
    */
    handleDateChange() {
        this.setState({ date: new Date() })
    }

    //c'est pour le chat
    sendMessage() {
        //sessionStorage.getItem("chatID");
        //console.log("-------------")
        //console.log(this.clientInfo)
        //console.log(this.listMate)
        //console.log(this.listChat)

        if (sessionStorage.getItem("conversationID") != 0) {
            let author = this.clientInfo.prenom;
            let date = new Date();
            let day = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
            let hour = date.getHours() + "h" + date.getMinutes()
            let time = { jour: day, heure: hour }
            let text = document.getElementById('content').value;
            if (text != "") {
                document.getElementById('content').value = "";
                this.socketHome.emit('sendMessage', text, author, time, sessionStorage.getItem("conversationID"));
            }
        }
        this.afficher()
    }

    afficher() {
        this.socketHome.on('receiveConversation', (conv) => {
            document.getElementById('messages').innerText = "";
            let checkUser
            conv.forEach(message => {
                if (checkUser != message[1]) {

                    let li = document.createElement('li');
                    let lo = document.createElement('li');


                    li.innerHTML = message[1] + " à " + message[2]
                    document.getElementById('messages').appendChild(li);
                    lo.innerHTML = message[0];
                    document.getElementById('messages').appendChild(lo);
                    checkUser = message[1]

                }
                else if (checkUser == message[1]) {
                    let li = document.createElement('li');
                    li.innerHTML = message[0]
                    document.getElementById('messages').appendChild(li);
                }
            });
        })
        this.socketHome.on('actualizeChat', (chatID)=>{
            this.socketHome.emit('showConversation', sessionStorage.getItem("clientID"), chatID)
        })
    }

    afficherForum() {
        this.socketHome.on('receiveConversationForum', (conv) => {
            document.getElementById('messages').innerText = "";
            let checkUser
            conv.forEach(message => {
                if (checkUser != message[1]) {

                    let li = document.createElement('li');
                    let lo = document.createElement('li');


                    li.innerHTML = message[1] + " à " + message[2]
                    document.getElementById('messages').appendChild(li);
                    lo.innerHTML = message[0];
                    document.getElementById('messages').appendChild(lo);
                    checkUser = message[1]

                }
                else if (checkUser == message[1]) {
                    let li = document.createElement('li');
                    li.innerHTML = message[0]
                    document.getElementById('messages').appendChild(li);
                }
            });
        })
        this.socketHome.on('actualizeForum', (chatID)=>{
            this.socketHome.emit('showConversationForum', sessionStorage.getItem("clientID"), chatID)
        })
    }


    onLoadClient() {
        //Si le client n'est pas logged in
        if (sessionStorage.getItem("clientID") == undefined) {
            window.location.href = "/Login";
        }

        //Reception des données dès qu'il arrive sur la page
        console.log("-------------")
        //
        //
        //
        this.socketHome.on('connect', () => {
            this.socketHome.emit("onHomeCo_clientInfo", sessionStorage.getItem("clientID"))
            this.socketHome.emit("onHomeCo_listMate", sessionStorage.getItem("clientID"))
            this.socketHome.emit("onHomeCo_listChat", sessionStorage.getItem("clientID"))
            //this.socketHome.emit("onHomeCo_listForum", sessionStorage.getItem("clientID"))
        });


        this.socketHome.on("receiveClientInfo", (data) => {

            if (data === undefined) {
                window.location.href = "/Login"
            } else {
                this.state.clientInfo = data

            }

        });

        this.socketHome.on('connect', () => {
            this.socketHome.emit("updateSocket", sessionStorage.getItem("clientID"), this.socketHome.id)
        })

        this.socketHome.on("receiveMateInfo", (data) => {
            let listMate = data
            //On trie la liste pour se retirer sois même
            listMate = listMate.filter(function (el) {
                return el.id !== sessionStorage.getItem("clientID");
            });
            this.setState({ listM: data })

        });

        this.socketHome.on("receiveChatInfo", (data) => {

            sessionStorage.setItem("listChat", JSON.stringify(data))

            this.setState({ listC: data })

        });

        this.socketHome.on("receiveForumInfo", (data) => {
            this.setState({ listF: data })

        });

        this.socketHome.on("retrySendInfo", () => {
            this.socketHome.emit("onHomeCo_clientInfo", sessionStorage.getItem("clientID"))
            setTimeout(function () { console.log("") }, 2000);
            this.socketHome.emit("onHomeCo_listMate", sessionStorage.getItem("clientID"))
            setTimeout(function () { console.log("") }, 2000);
            this.socketHome.emit("onHomeCo_listChat", sessionStorage.getItem("clientID"))
            setTimeout(function () { console.log("") }, 2000);
            this.socketHome.emit("onHomeCo_listForum", sessionStorage.getItem("clientID"))
        })
    }

    popChat() {
        console.log("Showing/removing chat");
        let x = document.getElementById('chatInput');
        let cal = document.getElementById("calend");

        if (x.style.display == "block") {
            x.style.display = "none";
            

        }
        else {
            x.style.display = "block";
            cal.style.display = "none"

        }
    }

    popConversations() {
        console.log("Showing/removing conversations");
        let bar = document.getElementById("chat");
        let x = document.getElementById("chatBar");
        let cal = document.getElementById("calend");
        let chattab=document.getElementById("tableMessages");
        let chatinput = document.getElementById("chatInput");
        if (x.style.display == "block") {
            x.style.display = "none";
            chattab.style.display ="none";
            chatinput.style.display = "none"

        } else {
            x.style.display = "block";
            cal.style.display = "none";
            chattab.style.display ="block";
            chatinput.style.display = "block";
        }
        var childrens = document.getElementsByClassName("chatListElem")[0].childNodes
        childrens.forEach(children => {
            children.onclick = () => {
                sessionStorage.setItem("conversationID", children.id.toString())
                this.socketHome.emit("showConversation", sessionStorage.getItem("clientID"), children.id.toString())
            }
        })
    }

    popForumsList() {
        console.log("Showing/removing forumList");
        let bar = document.getElementById("chat");
        let x = document.getElementById("chatBar");
        let cal = document.getElementById("calend");
        let foru = document.getElementById("");
        if (x.style.display == "block") {
            x.style.display = "none";
        } else {
            x.style.display = "block";
            cal.style.display = "none"
        }
    }



    popCalend() {
        console.log("Showing/removing chat");
        let x = document.getElementById("chatBar");
        let bar = document.getElementById("chat");
        let cal = document.getElementById("calend");
        if (cal.style.display === "block") {
            cal.style.display = "none";
        } else {
            cal.style.display = "block";
            x.style.display = "none";
            bar.style.display = "none";
        }
    }

    popNotes() {
        let postit = document.getElementById("post-It");
        if (postit.style.display === "none") {
            postit.style.display = "block";
        }
        else {
            postit.style.display = "none";
        }
    }

    popCreaChat() {
        let creaChat = document.getElementById("formCreaChat");
        document.getElementById("creaChat").value = "";
        if (creaChat.style.display === "block") {
            creaChat.style.display = "none";
        }
        else {
            creaChat.style.display = "block";
        }
    }

    onCreateChat(e) {
        e.preventDefault();
        let value = document.getElementById("creaChat").value;
        if (value != "") {
            this.socketHome.emit('requestCreateChat', value);
            setTimeout(function () { console.log("") }, 2000);
            window.location.href = "/"
            document.window.reload()
            this.popCreaChat();
        }
    }

    deconnexion(e) {
        e.preventDefault();
        sessionStorage.clear();
        window.location.href = "Login";
    }

    stateABS(e) {
        e.prevent.default();
    }

    //Posts-it
    onSave() {
        // Make sure to delete the editorState before saving to backend
        const notes = this.state.notes;
        notes.map(note => {
            delete note.editorState;
        })
        // Make service call to save notes
        // Code goes here...
    }
    onChange(notes) {
        this.setState({ // Update the notes state
            notes
        })
    }


    /*contextMenu() {
        //const [show, setShow] = useState(false);
        this.state = { shown: false };

        const handleClose = () => setShow(false);
        const handleShow = () => setShow(true);

        return (
            <>
                <Button variant="primary" onClick={handleShow}>
                    Launch demo modal
                </Button>

                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={handleClose}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }*/


    /*useEffect(() => {
        const interval = setInterval(
            () => onChange(new Date()),
            1000
        );
     
        return () => {
            clearInterval(interval);
        }
    }, []);*/

    render() {
        return (
            <div className="Home">
                <Container id="mainContainer" className="h-100" fluid>
                    <Row className="mainRow h-100">
                        <Col className="verticalNavBarLeft" xs={2}>
                            <Row id="contactsRow">
                                <Row id="contactText">
                                    <h4>Contacts</h4>
                                </Row>

                                <Row id="contactList">
                                    <div id="sb" class="table-responsive  table-wrapper-scroll-y my-custom-scrollbar">
                                        <table class="table table-fixed">
                                            <thead>
                                                <tr>
                                                    <div id="tableList"><ContactList nomPersonnage={this.state.listM} /></div>
                                                </tr>
                                            </thead>
                                            <tbody id="chat">
                                            </tbody>
                                        </table>
                                    </div>
                                </Row>

                                <Row id="profileStatus">
                                    <p>{this.state.clientInfo.prenom} {this.state.clientInfo.nom} </p>
                                    <Dropdown>
                                        <Dropdown.Toggle variant="info" id="dropdown-basic state" size="sm">
                                            <FontAwesomeIcon icon={faEye} /> État de connexion
                                        </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item id="connected"><FontAwesomeIcon icon={faToggleOn} id="connectedBtn" /> Connecté</Dropdown.Item>
                                            <Dropdown.Divider />
                                            <Dropdown.Item id="abs"><FontAwesomeIcon icon={faToggleOff} id="absentBtn" /> Absent</Dropdown.Item>
                                            <Dropdown.Divider />
                                            <Dropdown.Item id="DND"><FontAwesomeIcon icon={faBellSlash} id="dndBtn" /> Ne pas déranger</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    <Button variant="outline-danger" onClick={this.deconnexion} size="sm" block><FontAwesomeIcon icon={faPowerOff} /> Déconnexion</Button>
                                </Row>
                            </Row>
                        </Col>

                        <Col className="centerContainer">
                            <div id="chatDiv">
                                <div id="sb" class="table-responsive  table-wrapper-scroll-y my-custom-scrollbar">
                                    <table id="tableMessages" class="table table-hover table-dark table-fixed">
                                        <tbody id="messages"></tbody>
                                    </table>
                                </div>
                                <div className="chat" id="chatInput">
                                    <InputGroup className="messageBar">
                                        <FormControl
                                            placeholder="Saisissez un message"
                                            aria-label="Saisissez un message"
                                            aria-describedby="basic-addon2"
                                            id="content"
                                        />
                                        <InputGroup.Append>
                                            <Button variant="success" onClick={this.sendMessage}><FontAwesomeIcon icon={faPaperPlane} /></Button>
                                        </InputGroup.Append>
                                    </InputGroup>
                                </div>
                            </div>
                            <Draggable>
                                <div id="calend">
                                    <Calendar className="cal"
                                        onChange={this.handleDateChange}
                                        value={this.state.date}
                                    />
                                </div>
                            </Draggable>
                            <div id="post-It">
                                <ReactStickies
                                    notes={this.state.notes}
                                    onChange={this.onChange}
                                />
                            </div>
                        </Col>

                        <Col className="verticalNavBarChat" id="chatBar" xs={2}>
                            <Nav defaultActiveKey="/" className="flex-column">
                                <ChatList listChat={this.state.listC} onClick={this.popChat} />
                            </Nav>
                            <Button variant="outline-light" onClick={this.popCreaChat}><FontAwesomeIcon icon={faPlusSquare} /> Ajouter un canal</Button>
                            <Row id="formCreaChat" block>
                                <InputGroup >
                                    <FormControl
                                        placeholder="Nom"
                                        aria-label="Saisissez un message"
                                        aria-describedby="basic-addon2"
                                        id="creaChat"
                                    />
                                    <InputGroup.Append>
                                        <Button variant="warning" onClick={this.onCreateChat}><FontAwesomeIcon icon={faCheckCircle} /></Button>
                                    </InputGroup.Append>
                                </InputGroup>
                            </Row>
                        </Col>


                        <Col className="verticalNavBarRight" xs={2}>
                            <Row>
                                <Nav defaultActiveKey="/" className="flex-column">
                                    <Nav.Link onClick={this.popConversations}><FontAwesomeIcon icon={faComments}/>  Conversations</Nav.Link>
                                    <Nav.Link onClick={this.popCalend}><FontAwesomeIcon icon={faCalendarAlt}/> Planning</Nav.Link>
                                    <Nav.Link ><FontAwesomeIcon icon={faQuestion}/> Forum</Nav.Link>
                                    <Nav.Link onClick={this.popNotes}><FontAwesomeIcon icon={faStickyNote}/>  Post-it </Nav.Link>
                                </Nav>
                            </Row>
                            <Row>
                                <Draggable>
                                    <div className="clockStyle">
                                        <Clock value={this.state.date} />
                                    </div>
                                </Draggable>
                            </Row>
                            <Row>
                                <a className="embedBourse" href="https://cex.io"><img src="https://cex.io/widget/dark/240/btc-usd" width="240" height="120" /></a>
                            </Row>
                            <Row>
                                <Image src="./public/dysack.png" rounded />
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default Home;