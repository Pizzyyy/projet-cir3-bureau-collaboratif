import React from 'react';
import Popover from 'react-bootstrap/Popover';
import Row from 'react-bootstrap/Row';
import Button from "react-bootstrap/Button";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Nav from "react-bootstrap/Nav";
import Home from "./Home.js";

class ForumList extends React.Component {
    render() {
        const { listForum} = this.props;
        
        let listTEMP = new Array()
        let listTEMP2 
        listTEMP2 = listForum
        
        if (listTEMP2[0] === undefined){
            listTEMP = ["No forum found !"]
        }
        else{


            if (listTEMP2[0].question !== "No Forum found !"){
                listTEMP2.forEach(element => {
                    listTEMP.push(element.question);
                });


            }
            else
            {
    
                listTEMP2 = [{"users":{"nom":"Nom"}}]
                listTEMP = ["No Forum found !"]
            }
        }
        return (<div className="chatListElem">{Array.from(
            Array(listTEMP.length),
            (e, i) => {
                  
                return (               
                <OverlayTrigger
                    trigger="hover"
                    placement="left"
                    overlay={                             
                    <Popover>
                        <Popover.Title as="h3">{listTEMP2[i].question}  </Popover.Title>

                        <Popover.Content >
                        {listForum[i].status} <br/>
                        </Popover.Content>
                        
                    </Popover>
                }
            > 
                    <Nav.Link key={i} id={listTEMP2[i].id}> {listTEMP[i]}</Nav.Link>
                 
                </OverlayTrigger>
                )
            })}</div>)
    }
}

export default ChatListd