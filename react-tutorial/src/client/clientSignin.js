import io from 'socket.io-client';

let loc = 'http://localhost:8000'
const socket = io.connect(loc);

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
/*-------------------------REGISTER PART---------------------*/
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

//var connection = document.getElementById('connection');
//connection.addEventListener('click', connect);
var isResponseReceived = false
var isAuthSuccessful = false
var reason = "Nop"


socket.on("isSignInSuccessful",(bool) => {
    isAuthSuccessful = bool
    reason = reason
    isResponseReceived = true
})

function connect(){
    socket.emit('connected');
}

export function sendInfoToDB(){
    let password = document.getElementById("password").innerHTML;
    let email = document.getElementById("email").innerHTML;
    /*
    usr =        this.id         =  0
    this.nom        = "EXAMPLE"
    this.prenom     = "Example"
    this.email      = "example@dysack.com"
    this.entreprise = "example"
    this.tel        = "0102030405"
    this.status     = "En ligne"
    En ligne
    Inactif
    Ne pas deranger
    Hors ligne  
    this.role       =  "membre"
    admin
    membre
    this.mdp        = "azertyuiop"
    */
    socket.emit("registerTry",email,password,0,socket.id)
}


export function receiveAuthValidationFromDB(){
   return isAuthSuccessful
}

export function receiveReasonFromDB(){
   return reason
}
