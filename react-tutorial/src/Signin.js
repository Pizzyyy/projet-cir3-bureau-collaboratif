import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import "./Signin.css";
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Link } from 'react-router-dom';



import io from 'socket.io-client';



/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
/*-------------------------Signin PART------------------------*/
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

//var connection = document.getElementById('connection');
//connection.addEventListener('click', connect);





export default function Signin() {


  let loc = 'http://localhost:8000'
  const socket = io.connect(loc);

  sessionStorage.clear()
  var isAuthSuccessful = null
  var reason = null

  console.log("Page refreshed")



  socket.on("isSignInSuccessful", (bool, rsn) => {
    let isAuthSuccessful = bool
    let reason = rsn
    submitForm(isAuthSuccessful, rsn)
  })



  function sendInfoToDB(data) {
    console.log("Sending info to Server...")
    socket.emit("registerTry", data, socket.id)
  }



  const [firstname, setFirstname] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [entreprise, setEntreprise] = useState("");
  const [tel, setTel] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");



  function validateForm() {
    if (firstname.length > 0 && name.length > 0 && entreprise.length > 0 && tel.length > 0 && email.length > 0 && password.length > 0 && confirmPassword.length > 0) {
      if (password == confirmPassword) {
        return true
      }

    }
    return false;
  }

  function submitForm(isAuthSuccessful, reason) {
    if (validateForm()) {
      if (isAuthSuccessful) {
        window.location.href = "/Login";
      }
      else {
        alert(reason)
      }
    }
    else { window.location.href = "/Signin"; }

  }


  function handleClick(e) {
    e.preventDefault();
    let data = {
      nom: name,
      prenom: firstname,
      email: email,
      entreprise: entreprise,
      tel: tel,
      mdp: password,
    }
    sendInfoToDB(data);
  }


  return (
    <div id="signInForm">
      <div className="logo">
        <Row className="justify-content-center">
          <Col md="auto">
          </Col>
          <Col md="auto">
            <Image src="dysack.png" height="250" width="250" roundedCircle />
          </Col>
          <Col md="auto">
          </Col>
        </Row>
      </div>
      <div className="Signin">
        <Form>

          <Form.Group size="lg" controlId="firstname">
            <Form.Label>Prénom</Form.Label>
            <Form.Control
              autoFocus
              type="firstname"
              value={firstname}
              onChange={(e) => setFirstname(e.target.value)}
              placeholder="Nacim"
            />
          </Form.Group>


          <Form.Group size="lg" controlId="name">
            <Form.Label>Nom</Form.Label>
            <Form.Control
              type="name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="IHADDADENE"
            />
          </Form.Group>

          <Form.Group size="lg" controlId="entreprise">
            <Form.Label>Entreprise</Form.Label>
            <Form.Control
              type="entreprise"
              value={entreprise}
              onChange={(e) => setEntreprise(e.target.value)}
              placeholder="JUNIA - ISEN"
            />
          </Form.Group>

          <Form.Group size="lg" controlId="email">
            <Form.Label>Adresse e-mail </Form.Label>
            <Form.Control
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}      //Ce que l'utilisateur écrit
              placeholder="nacim.ihadadene@dysack.com"
            />
          </Form.Group>


          <Form.Group size="lg" controlId="tel">
            <Form.Label>Téléphone </Form.Label>
            <Form.Control
              type="tel"
              value={tel}
              onChange={(e) => setTel(e.target.value)}      //Ce que l'utilisateur écrit
              placeholder="06 XX XX XX XX"
            />
          </Form.Group>

          <Form.Group size="lg" controlId="password">
            <Form.Label>Mot de passe </Form.Label>
            <Form.Control
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              placeholder="Azerty1234"
            />
          </Form.Group>


          <Form.Group size="lg" controlId="confirmPassword">
            <Form.Label>Mot de passe, confirmation</Form.Label>
            <Form.Control
              type="password"
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
              placeholder="Azerty1234... encore !"
            />
          </Form.Group>
          <Button variant="outline-info" block size="lg" type="submit" onClick={handleClick} disabled={!validateForm()}>
            S'inscrire
            </Button>
        </Form>
      </div >
    </div >
  );
}