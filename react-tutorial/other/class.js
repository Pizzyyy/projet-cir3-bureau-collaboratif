class user{
    constructor(){
        this.id         =  0
        this.nom        = "EXAMPLE"
        this.prenom     = "Example"
        this.email      = "example@dysack.com"
        this.entreprise = "example"
        this.tel        = "0102030405"
        this.status     = "En ligne"
        /*
        En ligne
        Inactif
        Ne pas deranger
        Hors ligne  
        */
        this.role       =  "membre"
        /*
        admin
        membre
        */
        this.mdp        = "azertyuiop"

    }
}

class date{
    constructor(){
    this.jour = "12/12/2621",
    this.heure= "12h12"
    }
}

class message{
    constructor(){
        this.content = "any string"
        this.author = "Example"
        this.date = new date()
    }

}


class users{
    constructor(){
        this.id=0,
        this.nom="EXAMPLE0",
        this.prenom="Example0"
    }
}


class chat{

    constructor(){
        this.id         =  0
        this.nom        = "example"
        this.users      = [new users(),new users() ,new users()]
        this.message    = new message()

        }
}

class planning{
    constructor(){
        this.id         = 0
        this.titre      = "example"
        this.msg        = "example"
        this.users      = [new users(),new users() ,new users()]
        this.date       = new date()
        this.status     = "Fait"
        /*
        Fait
        En cours
        A faire
        */
    }
}