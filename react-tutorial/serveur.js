const { Socket } = require('dgram');
var express = require('express');
const { userInfo } = require('os');
const path = require('path')
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var uuid = require('uuid-random')
const DBMod = require("./server_modules/DatabaseModule.js")
const User = require("./server_modules/User")


var connectedUsers=new Map()

app.use(express.static(__dirname))

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/login.html');
})

app.get('/chat', function(req, res) {
    res.sendFile(__dirname + '/chat.html');
})



http.listen(8000, function(){
    console.log("server running");
})
io.sockets.on('connection', (socket)=>{
    //Creation d'un chat (a supprimer plus tard)
    
    let data ={

        nom:null,
        prenom:null,
        email:null,
        entreprise:"ISEN",
        tel:null,
        status:"Inactif",
        role:"Membre"
    } 

    var userInfo = new User(data)



    /*-------------------------------------------*/
    /*-------Partie pour le Signin----------------*/
    /*-------------------------------------------*/
    socket.on('registerTry', (usr,socketID)=> {


        if(usr.password != usr.confirmPassword){
            io.to(socketID).emit("isSignInSuccessful",false,"Les 2 mots de passes doivent correspondre")
        }
        else{

            DBMod.getListUsersEmail(usr.email).then(function(res)
            {
                if(res.length== 0) //Si il n'y a rien dans la BDD
                { 
                    //génère uuid de l'utilisateur
                    userID= uuid();
                    //on stocke l'email , le mdp + toute autres infos et on modifie le statut de l'utilisateur dans la bdd
                    usr.status = "En ligne";
                    usr.role ="Admin";
                    
                    DBMod.addUser(userID,usr.nom,usr.prenom,usr.email,usr.entreprise,usr.tel,usr.status,usr.role,usr.mdp);
                    canConnect=1;
                    io.to(socketID).emit("isSignInSuccessful",true,"");
                }
                else
                {
                    io.to(socketID).emit("isSignInSuccessful",false,"Ce compte existe déjà !");
                }

            })
        }
    })





    /*-------------------------------------------*/
    /*-------Partie pour le Login----------------*/
    /*-------------------------------------------*/
    socket.on('connectionTry', (email,mdp,socketID) =>{
        
        
        let userID;
        let canConnect=0;
        let tempMap=new Map([["socketID",null],["chatID",null]])

         //On recupere les infos de la base de donnée ( si tous est valide on le connecte)
         DBMod.getListUsersEmail(email).then(function(res) 
         {

             if(res.length== 0) //Si il n'y a rien dans la BDD
             { 

                io.to(socketID).emit("isAuthSuccessful",false,"Ce compte n'existe pas")
                  
             }
             else           
             {      //si il existe deja au sein de la base de donnée (etat 1) et qu'il essaye de se connecter
                    


                //On check son email et mdp
                if(res[0].mdp==mdp){
                        //On stocke l'uuid dans la map des utilisateurs connectés 
                        //On modifie le statut de l'utilisateur dans la bdd
                        userInfo.updateAllInfo(res[0])
                        userID = res[0].id;
                        tempMap.set("socketID",socketID)
                        connectedUsers.set(userID,tempMap);
                        canConnect=1

                        io.to(socketID).emit("isAuthSuccessful",true,"",userID)
                        //On stocke l'uuid dans la session(ou cookie)
                    }
                 
                 else{
                    io.to(socketID).emit("isAuthSuccessful",false,"Mauvais mot de passe",null)
                 }
             }
            
                 
         })
             
         
     

   

})





  /*-------------------------------------------*/
    /*--------Partie pour le Home----------------*/
    /*-------------------------------------------*/

  

    socket.on('requestCreateChat',(name)=>{//quand un utilisateur veut créer un chat (a implémenter)
        let chatID= uuid();
        let date = new Date();
        let groupName = name
        let day = date.getDate() + "/" + (date.getMonth() + 1 ) + "/" +date.getFullYear()
        let hour= date.getHours() + "h" + date.getMinutes()
        let creator = [{
            id:userInfo.id,
            nom:userInfo.nom,
            prenom:userInfo.prenom
        }]

        if(groupName == null){
            groupeName = "Default Chat"
        }
        DBMod.createChat(chatID,name,creator,day,hour);
    

    })
    socket.on('',(chatId,usrEmail)=>{//quand un utilisateur veut créer un chat (a implémenter)
        
        if(usrEmail !== null){

            DBMod.getListUsersEmail(usrEmail).then(function(res) 
            {
                if(res.length == 0){
                    //SHOULD SEND TO CLIENT THAT THE EMAIL HE PROVIDED IS NOT IN DB
                }
                else{
                    data = {
                        id : res[0].id,
                        nom : res[0].nom,
                        prenom : res[0].prenom
                    }
                    DBMod.addUserToChat(chatId,data)
                }
            })
        }
    })
    
    socket.on('sendForum', (text, userID, time, forumID) => {
        let message = {}
        message.content = text;
        message.author = userID;
        message.date = time;
        DBMod.addMsgToForum(forumID, message)
        let temp=[]
        let tempForum=[]
        DBMod.getChatById(ForumID).then(function (res) {//resultat de la requête dans res
            if(res!=null){

            connectedUsers.forEach( (value, key, map,tempForum) => {
                if (value.get("forumID") != null) {
                    //check si l'utilisateur est connecté
                    //check si l'utilisateur a un socket associé
                    if (value.get("socketID") != null) {
                        io.to(value.get("socketID")).emit('actualizeForum',chatID)//Envoyer le message
                    }

                }
            })
        }
        //Actualiser la database
        DBMod.addMsgToForum(forumID, message)

    })
    })


    socket.on('requestShowForum', (userID, forumID) => {
        //affiche un forum spécifique 
        //demande de changer le focus d'un user sur un forum
    })
    socket.on('requestCreateForum', (userID/*infos */) => {
        //demande de créer un forum
    })

    socket.on('requestMessageForum', (userID/*infos */) => {
        //demande d'envoyer un message sur un forum
    })
    socket.on('updateSocket',(userID,socketID)=>{//quand un utilisateur change de page on stocke sa nouvelle socket(a faire) utile???
        if(connectedUsers.get(userID) != undefined){
            let tempMap=connectedUsers.get(userID)
        
        //Modifier la socket dans le tableau par rapport à l'userID

        
            if(tempMap.get("socketID") != undefined){
                tempMap.set("socketID",socketID)
                connectedUsers.set(userID,tempMap)
            }
        }
    })
  


    socket.on(' changingChat', (socketID, chatID, userID) => {
        let tempMap = connectedUsers.get(userID)
        tempMap.set("chatID", chatID)
        connectedUsers.set(userID, tempMap)
        //update l'user

    })
    socket.on('sendMessage', (text, userID, time, chatID) => {
        let message = {}
        message.content = text;
        message.author = userID;
        message.date = time;
        let temp=[]
        let tempChat=[]
        DBMod.getChatById(chatID).then(function (res) {//resultat de la requête dans res
            if(res!=null){

            connectedUsers.forEach( (value, key, map,tempChat) => {
                if (value.get("chatID") != null) {
                    //check si l'utilisateur est connecté
                    //check si l'utilisateur a un socket associé
                    if (value.get("socketID") != null) {
                        io.to(value.get("socketID")).emit('actualizeChat',chatID)//Envoyer le message
                    }

                }
            })
        }
        //Actualiser la database
        DBMod.addMsgToChat(chatID, message)

    })
    })

   socket.on('showConversation', (userID, convID) => {  //requête avec un certain ID
        var tempChat = [];
        let temp = [];
        let tempMap=new Map()
        if (connectedUsers.get(userID) != null) {
            
            
               
        DBMod.getChatById(convID).then(function (res) {//resultat de la requête dans res
            
            if(res!=null){
                let inChat=0;
                res.users.forEach((elt)=>{
                    
                    if(elt.id==userID){
                        inChat=1;
                        
                    }
                })
                
                if(inChat){
            tempMap=connectedUsers.get(userID)
            tempMap.set("chatID",convID)
            connectedUsers.set(userID,tempMap)
            res.messages.forEach(elt => {
                temp = []

                temp.push(elt.content)
                temp.push(elt.author)
                temp.push(elt.date.heure)
                tempChat.push(temp)

            })
            
            tempMap=connectedUsers.get(userID)
            io.to(tempMap.get("socketID")).emit('receiveConversation', tempChat);
            
        }
    }
        })
        /*if (connectedUsers.get(userID) != null) {
            console.log("alexandre trop nul")
            if (connectedUsers.get(userID).chatID == convID) {

                // console.log("alexandre vraiment trop nul")
          
            */
        }
    
    })

    socket.on('showConversationForum', (userID, forumID) => {  //requête avec un certain ID
        var tempForum = [];
        let temp = [];
        let tempMap=new Map()
        if (connectedUsers.get(userID) != null) {
            
            
               
        DBMod.getChatById(forumID).then(function (res) {//resultat de la requête dans res
            
            if(res!=null){
                let inForum=0;
                res.users.forEach((elt)=>{
                    
                    if(elt.id==userID){
                        inForum=1;
                        
                    }
                })
                
                if(inForum){
            tempMap=connectedUsers.get(userID)
            tempMap.set("forumID",convID)
            connectedUsers.set(userID,tempMap)
            res.messages.forEach(elt => {
                temp = []

                temp.push(elt.content)
                temp.push(elt.author)
                temp.push(elt.date.heure)
                tempChat.push(temp)

            })
            
            tempMap=connectedUsers.get(userID)
            io.to(tempMap.get("socketID")).emit('receiveConversationForum', tempForum);
            
        }
    }
        })
        /*if (connectedUsers.get(userID) != null) {
            console.log("alexandre trop nul")
            if (connectedUsers.get(userID).chatID == convID) {

                // console.log("alexandre vraiment trop nul")
          
            */
        }
    
    })



    socket.on("requestModifyPlanning",()=>{

    })

    socket.on("onHomeCo_clientInfo",(userID)=>{
        DBMod.getUserById(userID).then(function(res){
            if(res == null) //Si il n'y a rien dans la BDD
            { 
                clientInfo = undefined  
                socket.emit("receiveClientInfo",clientInfo)
                              
            }
            else           
            {      //si il existe deja au sein de la base de donnée
                clientInfo = res;
                userInfo.updateAllInfo(res)
                socket.emit("receiveClientInfo",clientInfo)
            }       
        })

    });

    socket.on("onHomeCo_listChat",(userID)=>{
        if(userID !== undefined){
            DBMod.getListUsersChat(userID).then(function(res){
                if(res == null) //Si il n'y a rien dans la BDD
                { 
                    listChat = "No chat are found"  
                    socket.emit("receiveChatInfo",listChat)
                                  
                }
                else           
                {      //si il existe deja au sein de la base de donnée
                    listChat = res;
                    socket.emit("receiveChatInfo",listChat)
                }       
            })
        }
        else{
            console.log("userInfo is undefined on server side")
        }

    });

    
    socket.on("onHomeCo_listForum",()=>{
        if (userInfo.id != undefined){
            DBMod.getListForumByEntreprise(userInfo.entreprise).then(function(res){
            
                if(res == null) //Si il n'y a rien dans la BDD
                { 
                    listForum = undefined  
                    socket.emit("receiveForumInfo",listForum)
                                
                }
                else           
                {      //si il existe deja au sein de la base de donnée
                    listForum = res;
                    socket.emit("receiveForumInfo",listForum)
                }       
            })
        }
        })


    socket.on("onHomeCo_listMate",()=>{
        if (userInfo.id != undefined){
            DBMod.getListUsersByEntreprise(userInfo.entreprise).then(function(res){
            
                if(res == null) //Si il n'y a rien dans la BDD
                { 
                    listMate = undefined  
                    socket.emit("receiveMateInfo",listMate)
                                  
                }
                else           
                {      //si il existe deja au sein de la base de donnée
                    listMate = res;
                    socket.emit("receiveMateInfo",listMate)
                }       
            })
        }
        else{
            socket.emit("retrySendInfo")
        }
    })
/*
    socket.on("onHomeCo_listChat",()=>{

        DBMod.getListUsersByEntreprise(userInfo.entreprise).then(function(res){
            
            if(res == null) //Si il n'y a rien dans la BDD
            { 
                listMate = undefined  
                socket.emit("receiveMateInfo",listMate)
                              
            }
            else           
            {      //si il existe deja au sein de la base de donnée
                listMate = res;
                socket.emit("receiveMateInfo",listMate)
            }       
        })
    })
    */
    socket.on('disconnect', (userID) => {
        connectedUsers.delete(userID)
        io.emit('disconnection', 'User disconnected')
    });

})
